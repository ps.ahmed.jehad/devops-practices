FROM openjdk:8
LABEL Ahmed Jehad (ahmadns@hotmail.com)
EXPOSE 8090
COPY target/assignment-*.jar /usr/local/app.jar
ENTRYPOINT java -jar -Dspring.profiles.active=mysql /usr/local/app.jar